package com.trashysofts.auct


import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.register_layout.*

class RegisterActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.register_layout)
        auth = FirebaseAuth.getInstance()
        button_signup_reglayout.setOnClickListener {
            signUpUser()
        }

    }

    private fun signUpUser() {
        if (textedit_email_reg.text.toString().isEmpty()) {
            textedit_email_reg.error = "Please enter email"
            textedit_email_reg.requestFocus()
            return
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(textedit_email_reg.text.toString()).matches()) {
            textedit_email_reg.error = "Please enter valid email"
            return
        }
        if (textedit_password_reg.text.toString().isEmpty()) {
            textedit_password_reg.error = "Please enter password"
            textedit_password_reg.requestFocus()
            return

        }
        auth.createUserWithEmailAndPassword(
            textedit_email_reg.text.toString(),
            textedit_password_reg.text.toString()
        )
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    startActivity(Intent(this, LoginActivity::class.java))
                    finish()
                } else {
                    Toast.makeText(
                        baseContext, "Sign-up failed.",
                        Toast.LENGTH_SHORT
                    ).show()

                }

            }
    }


}

